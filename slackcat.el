;;; slackcat.el --- A slack interface -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Thomas Stenersen
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; SPDX-License-Identifier: GPL-3.0-or-later

;; Author: Thomas Stenersen <stenersen.thomas@gmail.com>
;; Maintainer: Thomas Stenersen <stenersen.thomas@gmail.com>
;; Created: september 07, 2022
;; Modified: september 07, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/thomas/slackcat
;; Package-Requires: ((emacs "24.3"))

;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  A slack interface for Emacs.
;;
;;; Code:

(require 'request)
(require 'json)
(require 's)

(defgroup slackcat-group nil
  "Slackcat configuration group."
  :group 'applications)


(defcustom slackcat-api-token
  (getenv "SLACK_API_TOKEN")
  "API token used to interact with the Slack REST API."
  :type '(string)
  :group 'slackcat-group)

(defcustom slackcat-conversations-cache-file
  (concat (file-name-directory (symbol-file 'slackcat-api-token))
          "conversations-cache.json")
  "Cached conversation list."
  :type '(file)
  :group 'slackcat-group)

(defconst slackcat-api
  "https://slack.com/api"
  "URL to Slack REST API.")

(defvar slackcat--user-list
  nil
  "User list loaded from `slackcat-conversations-cache-file'.")

(cl-defun slackcat--on-success (&key data &allow-other-keys)
  "Callback function for successful requests.

DATA is passed from 'request."
  (if (not (cdr (assoc 'ok data)))
      (message "Error sending message: %s" (cdr (assoc 'error data)))))

(cl-defun slackcat--on-error (&key error-thrown &allow-other-keys)
    "Callback function for erroneuos requests.

ERROR-THROWN is passed from 'request."
  (message "Error %S" error-thrown))

(defun slackcat--post-message (message channel-id)
  "Sent MESSAGE to CHANNEL-ID."
  (request (concat slackcat-api "/chat.postMessage")
    :type "POST"
    :data (json-encode (list `("channel" . ,channel-id)
                             `("text" . ,message)
                             '(as_user . true)))
    :headers (list
              '("Content-Type" . "application/json; charset=utf-8")
              `("Authorization" . ,(concat "Bearer " slackcat-api-token)))
    :encoding 'utf-8
    :success 'slackcat--on-success
    :error 'slackcat--on-error
    :parser 'json-read))

(defun slackcat--do-send ()
  "Given a filled buffer, this command will post contents as a slack message."
  (interactive)
  (with-current-buffer "*slackcat-message*"
    (let* ((msg (buffer-string))
           (conversation (completing-read "To: "
                                          (mapcar (lambda (c)
                                                    (format "%s [%s]"
                                                            (cdr (assoc 'real_name c))
                                                            (cdr (assoc 'id c))))
                                                  slackcat--user-list)))
           (conversation-id (nth 1 (s-match (rx "[" (group (one-or-more (any alnum))) "]")
                                            conversation))))
      (slackcat--post-message msg conversation-id)))
  (kill-buffer "*slackcat-message*"))

(defun slackcat-send (&optional msg)
  "Send message MSG interactively."
  (interactive (list (if (region-active-p)
                         (buffer-substring (region-beginning)
                                           (region-end))
                       nil)))
  (let ((substring msg))
    (with-current-buffer (get-buffer-create "*slackcat-message*")
      (if msg
          (insert (format "```\n%s\n```\n" substring)))
      (local-set-key (kbd "C-c C-c") #'slackcat--do-send)))
  (pop-to-buffer "*slackcat-message*"))

;; TODO: Improve user list handling.
;;
;; - Add function to retrieve conversation list from Slack API and populate user
;; - list cache.
(defun slackcat-load-user-list ()
  "Load the conversation cache."
  (interactive)
  (setq slackcat--user-list (json-read-file slackcat-conversations-cache-file)))

(provide 'slackcat)
;;; slackcat.el ends here
